(function (angular) {
  'use strict';

  angular.module('coyo.custom', [
    'coyo.app',
    'coyo.custom.apps.example',
    'coyo.custom.widgets.example'
  ]).run(function ($log) {
    $log.debug('Starting custom coyo application...');
  });

})(angular);
