(function (angular) {
  'use strict';

  // eslint-disable-next-line angular/service-name
  angular
      .module('coyo.custom.apps.example')
      .factory('ExampleModel', ExampleModel);

  /**
   * @ngdoc service
   * @name coyo.custom.apps.example.ExampleModel
   *
   * @description
   * Domain model representation of the example endpoint. Creates a new ExampleModel object.
   *
   * @requires coyo.apps.api.appResourceFactory
   */
  function ExampleModel(appResourceFactory) {
    var Example = appResourceFactory({
      appKey: 'example',
      url: '/example',
      name: 'example'
    });
    
    // class members
    angular.extend(Example, {
      /**
       * @ngdoc function
       * @name coyo.custom.apps.example.ExampleModel#save
       * @methodOf coyo.custom.apps.example.ExampleModel
       *
       * @description
       * Saves a new message.
       *
       * @returns {promise} An $http promise
       */
      save: function (app, message) {
        return this.$http({
          method: 'POST',
          url: this.$url({
                senderId: app.senderId,
                appId: app.id
              }),
          data: {
            message: message
          }
        });
      }
    });

    return Example;
  }

})(angular);
