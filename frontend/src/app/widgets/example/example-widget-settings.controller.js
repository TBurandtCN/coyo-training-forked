(function (angular) {
  'use strict';

  angular
      .module('coyo.custom.widgets.example')
      .controller('ExampleWidgetSettingsController', ExampleWidgetSettingsController);

  function ExampleWidgetSettingsController($log, $scope) {
    var vm = this;
    vm.foo = 'bar';
    $log.debug('[ExampleWidgetSettingsController] vm, $scope', vm, $scope);
  }

})(angular);
