(function (angular) {
  'use strict';

  angular
      .module('coyo.custom.widgets.example')
      .directive('exampleWidget', exampleWidget)
      .controller('ExampleWidgetController', ExampleWidgetController);

  function exampleWidget() {
    return {
      restrict: 'E',
      templateUrl: 'app/widgets/example/example-widget.html',
      scope: {},
      bindToController: {
        widget: '='
      },
      controller: 'ExampleWidgetController',
      controllerAs: '$ctrl'
    };
  }

  function ExampleWidgetController() {
  }

})(angular);
