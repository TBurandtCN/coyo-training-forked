package com.mindsmash.coyo.custom.apps.example;

import com.mindsmash.coyo.data.domain.apps.App;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service for handling example app actions.
 */
@Service
public class ExampleService {

    @Autowired
    private ExampleRepository exampleRepository;

    /**
     * Returns the example app message.
     *
     * @param app The app
     * @return the example app message
     */
    public Example getExample(App app) {
        return exampleRepository.findByApp(app).orElse(null);
    }

    /**
     * Sets the example app message.
     *
     * @param app The app
     * @param requestBody Request body containing the message
     */
    public void setMessage(App app, ExampleRequestBody requestBody) {
        Example example = exampleRepository.findByApp(app).orElse(Example.builder().app(app).build());
        example.setMessage(requestBody.getMessage());
        exampleRepository.save(example);
    }
}
