package com.mindsmash.coyo.custom.apps.example;

import lombok.Getter;
import lombok.Setter;

/**
 * The request body for an example app.
 */
@Getter
@Setter
public class ExampleRequestBody {

    private String message;
}
