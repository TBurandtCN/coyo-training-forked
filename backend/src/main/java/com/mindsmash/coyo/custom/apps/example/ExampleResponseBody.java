package com.mindsmash.coyo.custom.apps.example;

import lombok.Getter;

/**
 * The response body for an example app.
 */
@Getter
public class ExampleResponseBody {

    private String message;

    public ExampleResponseBody(Example example) {
        this.message = example.getMessage();
    }
}
