# Coyo4 Scaffolding Project

This repository contains a sample project setup for a Coyo4 project. It is intended to be forked by developers.

**[For details, please check out the Coyo4 developer guides](http://beta.coyo4.com/docs)**.